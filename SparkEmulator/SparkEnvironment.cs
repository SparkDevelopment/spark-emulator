﻿using HabboEncryption;
using HabboEncryption.Keys;
using SparkEmulator.Spark;
using SparkEmulator.Spark.Client;
using SparkEmulator.Spark.Database;
using SparkEmulator.Spark.Messages;
using SparkEmulator.Spark.Network;
using SparkEmulator.Spark.Packets;
using SparkEmulator.Spark.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace SparkEmulator
{
    class SparkEnvironment
    {
        public static readonly string VERSION = "0.1 Alpha";

        public static DatabaseConnection Database { get; private set; }
        public static Registery Registery { get; private set; }
        public static SocketServer SocketServer { get; private set; }
        public static Game Game { get; private set; }
        public static bool Running { get; private set; }
        public static bool Restart { get; private set; }

        /// <summary>
        /// Initialize and start the SparkEnvironment.
        /// </summary>
        public static void Init()
        {
            // Resize the console.
            Console.Title = "SparkEmu " + VERSION;
            Console.WindowWidth = 120;
            Console.WindowHeight = 36;

            // Show some information.
            Logger.Info("Starting up SparkEmu...");
            Logger.Debug("Running version: " + VERSION);

            // Set default variables.
            SparkEnvironment.Running = true;
            SparkEnvironment.Restart = false;

            // Init the crypto.
            HabboCrypto.Initialize(new RsaKeyHolder());

            // Load the database.
            // TODO Database settings.
            SparkEnvironment.Database = new DatabaseConnection("Server=localhost;Database=emulator;Uid=root;Pwd=test;");
            SparkEnvironment.Database.Connect();

            // Load the registery.
            SparkEnvironment.Registery = new Registery();
            SparkEnvironment.Registery.Search();

            // Load the socket server and start listening.
            SparkEnvironment.SocketServer = new SocketServer();
            SparkEnvironment.SocketServer.Listen();

            // Load the game.
            SparkEnvironment.Game = new Game();

            // Listen to the console input.
            Listen();

            // Restart if needed.
            if (Restart)
            {
                System.Diagnostics.Process.Start(System.Reflection.Assembly.GetExecutingAssembly().Location);
            }
        }

        /// <summary>
        /// Listen to the console input.
        /// </summary>
        private static void Listen()
        {
            Logger.Info("Press [Enter] to perform an command.");

            while (Running)
            {
                if (Console.ReadKey(true).Key == ConsoleKey.Enter)
                {
                    string command = Logger.Read();

                    switch (command)
                    {
                        case "shutdown":
                            SparkEnvironment.Shutdown();
                            break;

                        case "restart":
                            SparkEnvironment.Shutdown(true);
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Stop the server from running. 
        /// Notice: This wil take an time.
        /// </summary>
        public static void Shutdown(bool restart = false)
        {
            Logger.Info("Server is shutting down... Finishing last cycle and saving data...");
            SparkEnvironment.SocketServer.Stop();
            SparkEnvironment.Running = false;
            SparkEnvironment.Restart = restart;
        }
    }
}
