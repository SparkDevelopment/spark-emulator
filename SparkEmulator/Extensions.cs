﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

public static class ExtensionMethods
{
    /// <summary>
    /// Combine two byte[] arrays.
    /// </summary>
    /// <param name="first"></param>
    /// <param name="second"></param>
    /// <returns></returns>
    public static T[] Add<T>(this T[] first, T[] second)
    {
        if (second.Length == 0)
        {
            return first;
        }
        else
        {
            T[] ret = new T[first.Length + second.Length];
            Buffer.BlockCopy(first, 0, ret, 0, first.Length);
            Buffer.BlockCopy(second, 0, ret, first.Length, second.Length);
            return ret;
        }
    }

    /// <summary>
    /// Clear the MemoryStream buffer.
    /// </summary>
    /// <param name="source"></param>
    public static void Clear(this MemoryStream source)
    {
        byte[] buffer = source.GetBuffer();
        Array.Clear(buffer, 0, buffer.Length);
        source.Position = 0;
        source.SetLength(0);
    }

    /// <summary>
    /// Returns the string in a fixed length. Used for console.
    /// </summary>
    /// <param name="str"></param>
    /// <param name="size">Size</param>
    /// <returns></returns>
    public static string FixedLength(this string str, int size)
    {
        if (size - str.Length > 0)
        {
            return str + (new String(' ', size - str.Length));
        }
        else if (str.Length > size)
        {
            return str.Substring(0, size);
        }
        else
        {
            return str;
        }
    }

    /// <summary>
    /// A shorter 
    /// </summary>
    /// <param name="stream"></param>
    /// <param name="bytes"></param>
    public static void Write(this MemoryStream stream, byte[] bytes)
    {
        stream.Write(bytes, 0, bytes.Length);   
    }
}