﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkEmulator.Spark.Database
{
    class DatabaseConnection
    {
        private MySqlConnection connection;
        public bool Connected { get; set; }

        public DatabaseConnection(string connectionString)
        {
            this.connection = new MySqlConnection(connectionString);
        }

        public void Connect()
        {
            try
            {
                this.connection.Open();
                this.Connected = true;
                Logger.Info("Connected to the MySQL Server.");
            }
            catch (Exception ex)
            {
                Logger.FatalException("Couldn't connect with the MySQL Server.", ex);
            }
        }

        public void Close()
        {
            if (Connected)
            {
                this.connection.Close();
            }
        }

        public MySqlCommand Query(string query)
        {
            return new MySqlCommand(query, connection);
        }

        public T[] FindAll<T>(MySqlCommand command) where T : DatabaseObject, new()
        {
            List<T> results = new List<T>();
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                T dbObject = new T();
                dbObject.Read(reader);
                results.Add(dbObject);
            }
            reader.Close();
            return results.ToArray();
        }

        public T Find<T>(MySqlCommand command) where T : DatabaseObject, new()
        {
            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                T dbObject = new T();
                dbObject.Read(reader);
                reader.Close();
                return dbObject;
            }
            else
            {
                return null;
            }
        }
    }
}
