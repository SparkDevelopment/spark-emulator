﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkEmulator.Spark.Database
{
    abstract class DatabaseObject
    {
        public bool UpdateNeeded { get { return updates.Count > 0; } }

        private Dictionary<string, object> values = new Dictionary<string,object>();
        private List<string> updates = new List<string>();

        public void Read(MySqlDataReader reader)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                values.Add(reader.GetName(i), reader[i]);
            }
        }

        public void Update()
        {
            if (UpdateNeeded)
            {
                List<string> where = new List<string>();
                updates.ForEach(k => where.Add(k + " = @" + k));
                string query = "UPDATE " + TableName() + " SET " + string.Join(", ", where.ToArray()) + " WHERE id = @id";
                
                using (var command = SparkEnvironment.Database.Query(query))
                {
                    command.Prepare();
                    command.Parameters.AddWithValue("@id", values["id"]);
                    updates.ForEach(k => command.Parameters.AddWithValue("@" + k, values[k]));
                    command.ExecuteNonQuery();
                    updates.Clear();
                }
            }
        }

        /// <summary>
        /// The table name for this class.
        /// </summary>
        /// <returns></returns>
        public abstract string TableName();

        /// <summary>
        /// The getter and setter for this class.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public object this[string index]
        {
            get
            {
                // Get the value.
                return values.ContainsKey(index) ? values[index] : null;
            }

            set
            {
                // Check if the index is in the values.
                if (values.ContainsKey(index))
                {
                    // It is, replace the value.
                    values[index] = value;
                }
                else
                {
                    // It isn't, add the index and value.
                    values.Add(index, value);
                }

                // Add the index to the update list.
                if (!updates.Contains(index))
                {
                    updates.Add(index);
                }
            }
        }
    }
}
