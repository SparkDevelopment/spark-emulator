﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

enum LogLevel
{
    Trace = 4,
    Debug = 3,
    Info = 2,
    Warning = 1,
    Error = 0
}

class LoggerMessage
{
    public DateTime Time;
    public LogLevel Level; 
    public string Name; 
    public object Obj;

    public LoggerMessage(DateTime time, LogLevel level, string name, object obj)
    {
        this.Time = time;
        this.Level = level;
        this.Name = name;
        this.Obj = obj;
    }
}

class Logger
{
    private static List<LoggerMessage> messages = new List<LoggerMessage>();
    private static bool listen = true;

    public static bool Listen
    {
        get
        {
            return listen;
        }
        set
        {
            if (value && messages.Count > 0)
            {
                foreach (LoggerMessage message in messages)
                {
                    WriteLine(message.Obj, message.Name, message.Time, message.Level);
                }
                messages.Clear();
            }

            listen = value;
        }
    }

    public static uint Level = 4;

    public static string Read()
    {
        Logger.Listen = false;
        Console.ForegroundColor = ConsoleColor.DarkYellow;
        Console.Write("[        Command         ] ");
        string output = Console.ReadLine();
        Console.ForegroundColor = ConsoleColor.White;
        Logger.Listen = true;
        return output;
    }

    public static void Debug(object obj)
    {
        WriteLine(obj, LogLevel.Debug);
    }

    public static void Info(object obj)
    {
        WriteLine(obj, LogLevel.Info);
    }

    public static void Warning(object obj)
    {
        WriteLine(obj, LogLevel.Warning);
    }

    public static void Error(object obj)
    {
        WriteLine(obj, LogLevel.Error);
    }

    public static void Trace(object obj)
    {
        WriteLine(obj, LogLevel.Trace);
    }

    private static void WriteLine(object obj, LogLevel level)
    {
        StackFrame frame = new StackFrame(2);
        string name = frame.GetMethod().DeclaringType.Name;
        int levelId = (int)level;

        // Insert the message into the database.
        if (levelId <= 1 && SparkEmulator.SparkEnvironment.Database.Connected)
        {
            using (var command = SparkEmulator.SparkEnvironment.Database.Query("INSERT INTO `logs` (`class`, `level`, `message`, `created`) VALUES (@class, @level, @message, NOW())"))
            {
                command.Parameters.AddWithValue("@class", name);
                command.Parameters.AddWithValue("@level", levelId);
                command.Parameters.AddWithValue("@message", obj.ToString());
                command.ExecuteNonQuery();
            }
        }

        // Check if the console can write.
        if (listen)
        {
            // Write the line.
            WriteLine(obj, name, DateTime.Now, level);
        }
        else
        {
            // Add to the list.
            messages.Add(new LoggerMessage(DateTime.Now, level, name, obj));
        }
    }

    private static void WriteLine(object obj, string name, DateTime dateTime, LogLevel level)
    {
        int levelId = (int)level;

        if (levelId <= Level)
        {
            Console.ForegroundColor = Color(level);
            Console.WriteLine("[" + dateTime.ToShortTimeString() + " | " + name.FixedLength(16) + "] " + obj);
        }
    }

    public static void Exception(string p, Exception ex)
    {
        // TODO Do something with the exception.
        Logger.Error(p + ": \n" + ex.StackTrace + "\n" + ex.Message);
    }

    public static void FatalException(string p, Exception ex)
    {
        // TODO shutdown the server. Log the error.
        Logger.Exception(p, ex);
    }

    public static ConsoleColor Color(LogLevel level)
    {
        switch (level)
        {
            case LogLevel.Trace:
                return ConsoleColor.Cyan;
            case LogLevel.Info:
                return ConsoleColor.White;
            case LogLevel.Warning:
                return ConsoleColor.Yellow;
            case LogLevel.Error:
                return ConsoleColor.Red;
            default:
                return ConsoleColor.Gray;
        }
    }
}