﻿using SparkEmulator.Spark.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkEmulator.Spark.Users
{
    class Habbo : Database.DatabaseObject
    {
        public static Dictionary<int, Habbo> habbosById = new Dictionary<int, Habbo>();
        public GameClient Client;

        /// <summary>
        /// Find the Habbo by the given field.
        /// </summary>
        /// <param name="value">The value</param>
        /// <param name="field">The field (default "username")</param>
        /// <param name="autoLoad">Must be loaded when it's not found (default true)</param>
        /// <returns></returns>
        public static Habbo Find(object value, string field = "username", bool autoLoad = true)
        {
            Habbo habbo = habbosById.FirstOrDefault(kv => kv.Value[field].Equals(value)).Value;

            if (habbo == null && autoLoad)
            {
                using (var command = SparkEnvironment.Database.Query("SELECT * FROM users WHERE " + field + " = @value LIMIT 0,1"))
                {
                    command.Prepare();
                    command.Parameters.AddWithValue("@value", value);

                    habbo = SparkEnvironment.Database.Find<Habbo>(command);
                    if (habbo != null)
                    {
                        habbosById.Add((int)habbo["id"], habbo);
                    }
                }
            }

            return habbo;
        }

        /// <summary>
        /// The table that this class uses.
        /// </summary>
        /// <returns></returns>
        public override string TableName()
        {
            return "users";
        }
    }
}
