﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SparkEmulator.Spark.Packets
{
    /// <summary>
    /// The Packet attribute is used on an method to register it as an Incoming or Outgoing packet-handler.
    /// </summary>
    [System.AttributeUsage(System.AttributeTargets.Method)]
    class Packet : System.Attribute
    {
        public int Id { get; private set; }
        public PacketType Side { get; private set; }

        public Packet(PacketType Side, int Id)
        {
            this.Id = Id;
            this.Side = Side;
        }

        public Packet(HabboHotel.Headers.Incoming Header)
        {
            this.Id = (int)Header;
            this.Side = PacketType.Incoming;
        }

        public Packet(HabboHotel.Headers.Outgoing Header)
        {
            this.Id = (int)Header;
            this.Side = PacketType.Outgoing;
        }
    }
}
