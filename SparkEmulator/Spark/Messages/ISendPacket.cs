﻿using SparkEmulator.Spark.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkEmulator.Spark.Messages
{
    /// <summary>
    /// The interface that's been used in GameClient and Room.
    /// </summary>
    interface ISendPacket
    {
        /// <summary>
        /// Called when the ServerMessage is ready to be sended.
        /// </summary>
        /// <param name="message">The ServerMessage</param>
        void Send(ServerMessage message);
    }
}
