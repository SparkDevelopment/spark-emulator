﻿using SparkEmulator.HabboHotel;
using SparkEmulator.HabboHotel.Headers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SparkEmulator.Spark.Messages
{
    /// <summary>
    /// The new ServerMessage with build-in queue.
    /// </summary>
    class ServerMessage
    {
        private MemoryStream Buffer;
        private MemoryStream TotalBuffer;
        private ISendPacket sendTo;

        public short Id;

        internal ServerMessage()
        {
            Buffer = new MemoryStream();
            TotalBuffer = new MemoryStream();
        }

        internal ServerMessage(ISendPacket sendTo)
            : this()
        {
            this.sendTo = sendTo;
        }

        internal void Init(int Header)
        {
            EndMessage();

            Id = (short)Header;
            AppendShort(Header);
        }

        internal void Append(Outgoing header, params object[] args)
        {
            int id = (int)header;

            if (SparkEnvironment.Registery.OutgoingPackets.ContainsKey(id))
            {
                this.Init(id);
                SparkEnvironment.Registery.OutgoingPackets[id].Invoke(null, (new object[] { this }).Add(args));
                this.EndMessage();
            }
            else
            {
                Logger.Warning("Failed to append id " + id);
            }
        }

        internal void AppendShort(int S)
        {
            Buffer.Write(HabboEncoding.EncodeInt16(S), 0, 2);
        }

        internal void AppendInt32(int i)
        {
            Buffer.Write(HabboEncoding.EncodeInt32(i), 0, 4);
        }

        internal void AppendUInt(uint i)
        {
            Buffer.Write(HabboEncoding.EncodeUInt(i), 0, 4);
        }

        internal void AppendBoolean(bool b)
        {
            AppendByte(b ? 1 : 0);
        }

        internal void AppendString(string str)
        {
            AppendShort(str.Length);
            Buffer.Write(Encoding.Default.GetBytes(str), 0, str.Length);
        }

        internal void AppendByte(int i)
        {
            Buffer.WriteByte((byte)i);
        }

        internal void Clear()
        {
            Buffer.Clear();
        }

        internal byte[] GetBytes()
        {
            EndMessage();
            return TotalBuffer.ToArray();
        }

        internal void EndMessage()
        {
            if (Buffer.Length > 0)
            {
                int length = (int)Buffer.Length;
                TotalBuffer.Write(HabboEncoding.EncodeInt32(length), 0, 4);
                TotalBuffer.Write(Buffer.GetBuffer(), 0, length);
                Buffer.Clear();
            }
        }

        internal void Send(ISendPacket target, bool clear = true)
        {
            EndMessage();

            if (TotalBuffer.Length > 0)
            {
                target.Send(this);

                if (clear)
                {
                    TotalBuffer.Clear();
                }
            }
        }

        internal void Send(bool clear = true)
        {
            Send(this.sendTo, clear);
        }
    }
}
