﻿using HabboEncryption;
using SparkEmulator.HabboHotel.Headers;
using SparkEmulator.Spark.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkEmulator.Spark.Messages
{
    class ServerMessageModels
    {
        [Packet(Outgoing.InitCryptoMessageComposer)]
        public static void InitCryptoMessageEvent(ServerMessage Response)
        {
            Response.AppendString(HabboCrypto.GetDHPrimeKey());
            Response.AppendString(HabboCrypto.GetDHGeneratorKey());
        }

        [Packet(Outgoing.SecretKeyMessageComposer)]
        public static void SecretKeyMessageComposer(ServerMessage Response)
        {
            Response.AppendString(HabboCrypto.GetDHPublicKey());
            Response.AppendBoolean(false);
        }
    }
}
