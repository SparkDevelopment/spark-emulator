﻿using SparkEmulator.HabboHotel;
using SparkEmulator.Spark.Messages;
using SparkEmulator.Spark.Packets;
using SparkEmulator.Spark.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace SparkEmulator.Spark.Client
{
    class GameClient : ISendPacket
    {
        private static byte[] policy = Encoding.Default.GetBytes("<?xml version=\"1.0\"?>\r\n<!DOCTYPE cross-domain-policy SYSTEM \"/xml/dtds/cross-domain-policy.dtd\">\r\n<cross-domain-policy>\r\n<allow-access-from domain=\"*\" to-ports=\"1-31111\" />\r\n</cross-domain-policy>\0");
        private static Dictionary<int, GameClient> gameClients = new Dictionary<int,GameClient>();
        private static int counter = 0;

        private byte[] buffer;

        public HabboEncryption.Hurlant.Crypto.Prng.ARC4 ARC4;
        public Habbo Habbo;

        public GameClientMessageHandler MessageHandler { get; private set; }
        public Socket Socket { get; private set; }
        public ServerMessage ServerMessage { get; private set; }

        /// <summary>
        /// Create a new GameClient from a Socket.
        /// </summary>
        /// <param name="socket"></param>
        /// <returns></returns>
        public static GameClient Create(Socket socket)
        {
            int id = counter++;
            GameClient client = new GameClient(socket);
            gameClients.Add(id, client);
            client.Listen();
            return client;
        }

        private GameClient(Socket socket)
        {
            this.buffer = new byte[1024];
            this.Socket = socket;

            this.ServerMessage = new ServerMessage(this);
            this.MessageHandler = new GameClientMessageHandler(this, ServerMessage);
        }

        /// <summary>
        /// Start listening to the client.
        /// </summary>
        public void Listen()
        {
            try
            {
                Socket.BeginReceive(this.buffer, 0, this.buffer.Length, SocketFlags.None, new AsyncCallback(receiveData), this);
            }
            catch(Exception ex)
            {
                Logger.Exception("Couldn't receive the message from the GameClient.", ex);
            }
        }

        /// <summary>
        /// Called when the server is receiving data from the Socket.
        /// </summary>
        /// <param name="iAr"></param>
        private void receiveData(IAsyncResult iAr)
        {
            int length = 0;
            try
            {
                length = Socket.EndReceive(iAr);
                byte[] packet = new byte[length];
                Array.Copy(this.buffer, packet, length);

                // Check if the length is higher then 0.
                // When the length is 0, the client is disconnected.
                if (length > 0)
                {
                    if (packet[0] == 60)
                    {
                        // Check if the client wants the XML-Policy
                        this.Send(policy);
                    }
                    else if (packet[0] != 67)
                    {
                        // Check if the client sended a packet.
                        handlePacketData(packet);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Exception("An GameClient received an exception while receiving data.", ex);
            }
            finally
            {
                // Check if the is still server is running and check if the client wants to disconnect.
                if (length != 0 && SparkEnvironment.Running)
                {
                    Listen();
                }
                else
                {
                    this.Disconnect();
                }
            }
        }

        /// <summary>
        /// Handeling packet data from the client.
        /// </summary>
        /// <param name="data"></param>
        private void handlePacketData(byte[] data)
        {
            int i = 0;
            if (this.ARC4 != null)
            {
                this.ARC4.Decrypt(ref data);
            }

            checked
            {
                int minLenght = data.Length - 4;
                while (i < data.Length && i < minLenght)
                {
                    try
                    {
                        int num = HabboEncoding.DecodeInt32(new byte[]
						{
							data[i++],
							data[i++],
							data[i++],
							data[i++]
						});
                        if (num >= 2 && num <= 1024)
                        {
                            int messageId = HabboEncoding.DecodeInt16(new byte[]
							{
								data[i++],
								data[i++]
							});

                            byte[] array = new byte[num - 2];
                            int num2 = 0;
                            while (num2 < array.Length && i < data.Length)
                            {
                                array[num2] = data[i++];
                                num2++;
                            }

                            this.MessageHandler.Request.Init(messageId, array);
                            if (SparkEnvironment.Registery.IncomingPackets.ContainsKey(messageId))
                            {
                                Logger.Trace("Handeling packet " + messageId);
                                SparkEnvironment.Registery.IncomingPackets[messageId].Invoke(this.MessageHandler, null);
                                ServerMessage.Send();
                            }
                            else
                            {
                                if (SparkEnvironment.Registery.IncomingPacketsNames.ContainsKey(messageId))
                                {
                                    Logger.Warning("Unknown packet " + messageId + " (" + SparkEnvironment.Registery.IncomingPacketsNames[messageId] + ")");
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Exception("Failed to parse packet.", ex);
                    }
                }
            }
        }
        
        /// <summary>
        /// Disconnect the GameClient from the server. 
        /// When the GameClient is already disconnected, it will returns false.
        /// </summary>
        /// <returns>Disconnected</returns>
        public bool Disconnect()
        {
            return false;
        }

        /// <summary>
        /// Send the ServerMessage to the GameClient.
        /// </summary>
        /// <param name="message">ServerMessage to be send.</param>
        public void Send(ServerMessage message)
        {
            this.Send(message.GetBytes());
        }

        /// <summary>
        /// Send the bytes to the client.
        /// </summary>
        /// <param name="packet"></param>
        public void Send(byte[] packet)
        {
            this.Socket.BeginSend(packet, 0, packet.Length, SocketFlags.None, sentData, null);
        }

        /// <summary>
        /// End sentData.
        /// </summary>
        /// <param name="iAr"></param>
        private void sentData(IAsyncResult iAr)
        {
            try
            {
                this.Socket.EndSend(iAr);
            }
            catch
            {
                this.Disconnect();
            }
        }
    }
}
