﻿using HabboEncryption;
using HabboEncryption.CodeProject.Utils;
using HabboEncryption.Hurlant.Crypto.Prng;
using SparkEmulator.HabboHotel.Headers;
using SparkEmulator.Spark.Client;
using SparkEmulator.Spark.Messages;
using SparkEmulator.Spark.Packets;
using SparkEmulator.Spark.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SparkEmulator.Spark.Client
{
    class GameClientMessageHandler
    {
        public ClientMessage Request;
        public ServerMessage Response;
        public GameClient Client;

        public GameClientMessageHandler(GameClient client, ServerMessage serverMessage)
        {
            this.Client = client;
            this.Request = new ClientMessage();
            this.Response = serverMessage;
        }

        /// <summary>
        /// Send the InitCryptoMessageComposer to the client.
        /// </summary>
        [Packet(Incoming.InitCryptoMessageEvent)]
        public void InitCryptoMessageEvent()
        {
            Response.Append(Outgoing.InitCryptoMessageComposer);
        }

        /// <summary>
        /// Calcuate the encryption key from the client and set it in the GameClient.
        /// </summary>
        [Packet(Incoming.GenerateSecretKeyMessageEvent)]
        public void GenerateSecretKeyMessageEvent()
        {
            string CipherKey = Request.PopFixedString();
            BigInteger SharedKey = HabboCrypto.CalculateDHSharedKey(CipherKey);

            if (SharedKey != 0)
            {
                Response.Append(Outgoing.SecretKeyMessageComposer);
                Client.ARC4 = new ARC4(SharedKey.getBytes());
            }
            else
            {
                Client.Disconnect();
            }
        }

        /// <summary>
        /// Receive the SSO-Ticket from the client.
        /// </summary>
        [Packet(Incoming.SSOTicketMessageEvent)]
        public void SSOTicketMessageEvent()
        {
            string ticket = Request.PopFixedString();
            Habbo habbo = Habbo.Find(ticket, "auth_ticket");

            if (habbo != null)
            {
                habbo.Client = Client;
                Client.Habbo = habbo;
                Logger.Debug("User " + habbo["username"] + " logged in.");
            }
        }
    }
}
