﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Linq;
using SparkEmulator.Spark.Client;
using SparkEmulator.Spark.Messages;
using SparkEmulator.HabboHotel.Headers;

namespace SparkEmulator.Spark.Packets
{
    class Registery
    {
        public Dictionary<int, string> IncomingPacketsNames;
        public Dictionary<int, MethodInfo> IncomingPackets;
        public Dictionary<int, MethodInfo> OutgoingPackets;

        public Registery()
        {
            this.IncomingPackets = new Dictionary<int, MethodInfo>();
            this.OutgoingPackets = new Dictionary<int, MethodInfo>();
            this.IncomingPacketsNames = new Dictionary<int, string>();
        }

        /// <summary>
        /// Search for all the Packets in te solution and add them to IncomingPackets/OutgoingPackets.
        /// </summary>
        public void Search()
        {
            // Get all the names for the id's.
            foreach (Incoming incoming in Enum.GetValues(typeof(Incoming)))
            {
                this.IncomingPacketsNames.Add((int)incoming, incoming.ToString());
            }

            // Search for the methods that has the attribute Packet.
            Assembly.GetExecutingAssembly().GetTypes()
                .SelectMany(t => t.GetMethods())
                .Where(m => m.GetCustomAttributes(typeof(Packet), false).Length > 0)
                .ToList().ForEach(i =>
                {
                    // Get the attribute packet.
                    Packet packet = (Packet)i.GetCustomAttributes(typeof(Packet), false).First();

                    // Check if the side is Incoming or Outgoing. And add it to the right dictionary.
                    if (packet.Side == PacketType.Incoming)
                    {
                        IncomingPackets.Add(packet.Id, i);
                    }
                    else
                    {
                        OutgoingPackets.Add(packet.Id, i);
                    }
                });

            // For debugging: show how many packets are loaded.
            Logger.Debug("Loaded " + IncomingPackets.Count + " incoming packets and " + OutgoingPackets.Count + " outgoing packets");
        }
    }
}
