﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SparkEmulator.Spark.Network
{
    class SocketServer
    {
        private ManualResetEvent allDone;
        private Socket listener;
        private IPEndPoint endPoint;
        private Thread thread;
        private bool listen;

        public SocketServer()
        {
            this.allDone = new ManualResetEvent(true);
            this.listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this.endPoint = new IPEndPoint(IPAddress.Any, 30004);
        }

        /// <summary>
        /// Start listening to the entire world.
        /// </summary>
        public void Listen()
        {
            try
            {
                this.listen = true;
                this.listener.Bind(endPoint);
                this.listener.Listen(100);
                
                this.thread = new Thread(new ThreadStart(WaitForNextConnection));
                this.thread.Name = "SparkEmulator - SocketServer";
                this.thread.Start();

                Logger.Info("Server started on port " + endPoint.Port);
            }
            catch (Exception ex)
            {
                Logger.Exception("Couldn't start the SocketServer", ex);
            }
        }

        /// <summary>
        /// Stop the SocketServer thread.
        /// </summary>
        public void Stop()
        {
            this.listen = false;
            this.allDone.Set();
        }

        /// <summary>
        /// Wait for the next connection.
        /// </summary>
        private void WaitForNextConnection()
        {
            try
            {
                // Wait for the next connection.
                allDone.Reset();
                listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
                allDone.WaitOne();
            }
            catch (Exception ex)
            {
                // An exception handled with the connection.
                Logger.Exception("Couldn't accept a new connection", ex);
            }
            finally
            {
                // Check if we're still listening.
                if (this.listen)
                {
                    // Wait for the next connection.
                    WaitForNextConnection();
                }
                else
                {
                    // Stop listening.
                    Logger.Info("Server stopped listening.");
                    listener.Close();
                }
            }
        }

        /// <summary>
        /// Called when a new connection appears (from the bush and then we summon Pikachu to finish him off!)
        /// </summary>
        /// <param name="ar"></param>
        private void AcceptCallback(IAsyncResult ar)
        {
            try
            {
                Socket connection = ((Socket)ar.AsyncState).EndAccept(ar);
                SparkEmulator.Spark.Client.GameClient.Create(connection);
            }
            catch (Exception ex)
            {
                Logger.Exception("Failed to accept an new connection.", ex);
            }
            finally
            {
                allDone.Set();
            }
        }
    }
}
