﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace SparkEmulator
{
    class Program
    {
        /// <summary>
        /// The startup function.
        /// </summary>
        /// <param name="args">Arguments</param>
        static void Main(string[] args)
        {
            SparkEnvironment.Init();
        }
    }
}
